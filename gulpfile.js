var gulp = require('gulp'),
    concat = require('gulp-concat'),
    autoPrefixer = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename');

gulp.task('build', () => {
    gulp.src([
        './src/libs/**/*.sass',
        './src/blocks/**/*.sass',
        './src/touch.blocks/**/*.sass',
    ])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(minifyCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./dist/css'));

    console.log('Сборка завершена.');
});

