$(function () {
    setTimeout(function () {
        $('.header__paint1').animate({
            top: '0%'
        }, 200);
    }, 200);

    setTimeout(function () {
        $('.header__paint2').animate({
            top: '0%'
        }, 200);
    }, 500);

    setTimeout(function () {
        $('.header__paint3').animate({
            top: '0%'
        }, 200);
    }, 800);

    setTimeout(function () {
        $('.header__paint4').animate({
            top: '0%'
        }, 200);
    }, 1100);

    setTimeout(function () {
        $('.header__paint5').animate({
            top: '0%'
        }, 200);
    }, 1400);

    $('.slider__items').slick({
        infinite: false,
        arrows: false,
        dots: true
    });

    $('a[href^="#"]').on('click', function () {
        if ($('#burger').hasClass('burger--state--active')) {
            $('#burger').toggleClass('burger--state--active');

            $('.header__menu').animate({
                left: '100%'
            }, 1000);

            $('.header__content').fadeIn();
            $('body').removeClass('g-disable-events');
        }

        var id = $(this).attr('href');

        $('html,body').animate({
            scrollTop: $(id).offset().top
        }, 500);
    });

    $('#burger').on('click', function (e) {
        e.preventDefault();

        if (! $(this).hasClass('burger--state--active')) {
            $(this).toggleClass('burger--state--active');

            $('.header__menu').animate({
                left: '0%'
            }, 1000);

            $('.header__content').fadeOut();
            $('body').addClass('g-disable-events');
        } else {
            $(this).toggleClass('burger--state--active');

            $('.header__menu').animate({
                left: '100%'
            }, 1000);

            $('.header__content').fadeIn();
            $('body').removeClass('g-disable-events');
        }
    });

    $(window).enllax();

    var man = $('#footer').waypoint({
        handler: function (direction) {
            $('.footer__man').animate({
                bottom: '0'
            }, 500);
        },
        offset: '90%'
    });

    var steps = $('#rules').waypoint({
        handler: function (direction) {
            setTimeout(function () {
                $('#stepone')
                    .show()
                    .addClass('animated bounceIn');
            }, 200);

            setTimeout(function () {
                $('#steptwo')
                    .show()
                    .addClass('animated bounceIn');
            }, 400);

            setTimeout(function () {
                $('#stepthree')
                    .show()
                    .addClass('animated bounceIn');
            }, 600);

            setTimeout(function () {
                $('#stepfour')
                    .show()
                    .addClass('animated bounceIn');
            }, 800);

            setTimeout(function () {
                $('#stepfive')
                    .show()
                    .addClass('animated bounceIn');
            }, 1000);

            setTimeout(function () {
                $('#stepsix')
                    .show()
                    .addClass('animated bounceIn');
            }, 1200);
        },
        offset: '70%'
    });

    var maps = $('#maps').waypoint({
        handler: function (direction) {
            setTimeout(function () {
                $('#maps')
                    .css({
                        opacity: '1'
                    })
                    .addClass('animated slideInUp');
            }, 200);
        },
        offset: '100%'
    });

    var maps = $('#slider').waypoint({
        handler: function (direction) {
            setTimeout(function () {
                $('#slider')
                    .css({
                        opacity: '1'
                    })
                    .addClass('animated slideInLeft');
            }, 200);
        },
        offset: '100%'
    });

    var result = $('#result').waypoint({
        handler: function (direction) {
            setTimeout(function () {
                $('#result')
                    .css({
                        opacity: '1'
                    })
                    .addClass('animated slideInRight');
            }, 200);
        },
        offset: '100%'
    });
});